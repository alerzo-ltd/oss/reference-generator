package generator

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestReferenceGenerator(t *testing.T) {
	service := "betting"
	channel := "customer"
	reference := ReferenceGenerator(service, channel)
	referenceTwo := ReferenceGenerator(service, channel)
	require.Equal(t, 20, len(reference))
	require.Equal(t, "ALP_BET_", reference[:8])
	require.Equal(t, 12, len(reference[8:]))
	require.NotEqual(t, reference, referenceTwo)
}

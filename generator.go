package generator

import (
	"crypto/rand"
	"fmt"
	"io"
)

// ReferenceGenerator This takes in the service name and the channel
// as an input and get a uniquely generated agent reference as a string
// The length of the generated reference code is 20
func ReferenceGenerator(service string, channel string) string {
	var table = [...]byte{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
		'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
		'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
		'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}
	var agentReference string
	maximumLength := 12
	prefix := referencePrefix[service][channel]
	referenceDigits := make([]byte, maximumLength)
	n, err := io.ReadAtLeast(rand.Reader, referenceDigits, maximumLength)
	if err != nil {
		fmt.Println(err)
	}
	if n != maximumLength {
		fmt.Println("not max")
	}
	for i := 0; i < len(referenceDigits); i++ {
		referenceDigits[i] = table[int(referenceDigits[i])%len(table)]
	}
	agentReference = fmt.Sprintf("%s%s", prefix, string(referenceDigits))
	return agentReference
}

# Reference Generator

Reference generator for alerzopay

## Getting started

To use this library, you need to pass in the service and the channel to generate a reference

### Example code

Run  ```go get gitlab.com/alerzo-ltd/oss/reference-generator```

```
package main

import (
    "fmt"

    generator "gitlab.com/alerzo-ltd/oss/reference-generator"
)

func main() {
    service := "betting"
    channel := "merchant"
    reference := generator.ReferenceGenerator(service, channel)

    fmt.Println(reference)
}

```
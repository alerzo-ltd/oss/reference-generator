package generator

var referencePrefix = map[string]map[string]string{
	"airtime": {
		"customer": "ALP_AIR_",
		"merchant": "ALPOS_AIR_",
		"business": "ALB2B_AIR_",
	},
	"data": {
		"customer": "ALP_DAT_",
		"merchant": "ALPOS_DAT_",
		"business": "ALB2B_DAT_",
	},
	"betting": {
		"customer": "ALP_BET_",
		"merchant": "ALPOS_BET_",
		"business": "ALB2B_BET_",
	},
	"bonusCredit": {
		"customer": "ALP_BOC_",
	},
	"bonusDebit": {
		"cutomer": "ALP_BOD_",
	},
	"cable": {
		"customer": "ALP_CAB_",
		"merchant": "ALPOS_CAB_",
		"business": "ALB2B_CAB_",
	},
	"commissionCredit": {
		"marchant": "ALPOS_CMC_",
		"business": "ALB2B_CMC_",
	},
	"commissionDebit": {
		"merchant": "ALPOS_CMD_",
		"business": "ALB2B_CMD_",
	},
	"electricity": {
		"customer": "ALP_ELE_",
		"merchant": "ALPOS_ELE_",
		"business": "ALB2B_ELE_",
	},
	"walletCredit": {
		"customer": "ALP_WAC_",
		"merchant": "ALPOS_WAC_",
		"business": "ALPOS_CMD_",
	},
	"walletDebit": {
		"customer": "ALP_WAD_",
		"merchant": "ALPOS_WAD_",
		"business": "ALB2B_WAD_",
	},
}
